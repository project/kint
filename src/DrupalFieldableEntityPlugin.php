<?php

declare(strict_types=1);

namespace Drupal\kint;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Kint\Parser\Parser;
use Kint\Parser\PluginBeginInterface;
use Kint\Parser\PluginCompleteInterface;
use Kint\Value\Context\ContextInterface;
use Kint\Value\Context\BaseContext;
use Kint\Value\Context\ClassOwnedContext;
use Kint\Value\InstanceValue;
use Kint\Value\UninitializedValue;
use Kint\Value\ArrayValue;
use Kint\Value\Representation\ContainerRepresentation;
use Kint\Value\Representation\ValueRepresentation;
use Kint\Value\AbstractValue;

/**
 * This plugin displays entities and fields more succinctly.
 */
class DrupalFieldableEntityPlugin implements PluginBeginInterface, PluginCompleteInterface {

  /**
   * The parser we're attached to.
   */
  private Parser $parser;

  /**
   * Whether the dump is currently in the plugin or not.
   *
   * This is important since we block rendering FieldItemList outside of the
   * plugin and need to explicitly allow it inside.
   */
  private bool $currentlyInPlugin = FALSE;

  /**
   * {@inheritDoc}
   */
  public function setParser(Parser $p): void {
    $this->parser = $p;
  }

  /**
   * {@inheritDoc}
   *
   * @return string[]
   *   List of gettype types this plugin will operate on.
   */
  public function getTypes(): array {
    return ['object'];
  }

  /**
   * {@inheritDoc}
   */
  public function getTriggers(): int {
    return Parser::TRIGGER_SUCCESS | Parser::TRIGGER_BEGIN;
  }

  /**
   * Blacklist any FieldItemList that we're not dumping explicitly.
   *
   * Where "explicitly" means in this plugin or directly by the user at the top
   * level.
   *
   * @see BlacklistPlugin::$shallow_blacklist
   */
  public function parseBegin(&$var, ContextInterface $c): ?AbstractValue {
    if ($var instanceof FieldItemListInterface && !$this->currentlyInPlugin && $c->getDepth()) {
      $b = new InstanceValue($c, \get_class($var), \spl_object_hash($var), \spl_object_id($var));
      /* @phpstan-ignore assign.propertyType */
      $b->flags |= AbstractValue::FLAG_BLACKLIST;
      return $b;
    }

    return NULL;
  }

  /**
   * Plugin for FieldableEntity and FieldItemList.
   */
  public function parseComplete(&$var, AbstractValue $v, int $trigger): AbstractValue {
    if ($v instanceof InstanceValue) {
      if ($var instanceof FieldItemListInterface) {
        return $this->parseFieldItemList($var, $v);
      }
      elseif ($var instanceof FieldableEntityInterface) {
        return $this->parseFieldableEntity($var, $v);
      }
    }

    return $v;
  }

  /**
   * Attach field contents to FieldItemList.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface<\Drupal\Core\Field\FieldItemInterface> $list
   *   The fields primary access point.
   * @param \Kint\Value\InstanceValue $v
   *   The value as parsed by kint thus far.
   */
  private function parseFieldItemList(FieldItemListInterface $list, InstanceValue $v): InstanceValue {
    $values = $list->getValue();

    // FieldItemListInterface doesn't specify getValue returns array, but the
    // concrete ItemList class does, so we need to confirm it's an array here.
    if (!is_array($values)) {
      return $v;
    }

    if (0 === \count($values)) {
      $rep = new ValueRepresentation(
        'No values',
        new UninitializedValue(new BaseContext('No values')),
        NULL,
        TRUE
      );
      $v->addRepresentation($rep, 0);
      return $v;
    }

    $cardinality = $list->getFieldDefinition()->getFieldStorageDefinition()->getCardinality();

    $ap = $v->getContext()->getAccessPath();

    if (1 === $cardinality) {
      $contents = [];

      foreach ($values[0] as $name => $value) {
        $context = new ClassOwnedContext($name, $v->getClassName());
        $context->depth = $v->getContext()->getDepth() + 1;
        if (NULL !== $ap) {
          $context->access_path = $ap . '->' . $name;
        }

        $contents[] = $this->parser->parse($value, $context);
      }

      if (\count($contents)) {
        $rep = new ContainerRepresentation('Field', $contents, 'entity_field', TRUE);
        $v->addRepresentation($rep, 0);
      }
    }
    else {
      $context = new ClassOwnedContext('getValue()', $v->getClassName());
      $context->depth = $v->getContext()->getDepth();
      if (NULL !== $ap) {
        $context->access_path = $ap . '->getValue()';
      }

      $out = $this->parser->parse($values, $context);

      if ($out instanceof ArrayValue && count($out->getContents())) {
        $rep = new ContainerRepresentation('Field', $out->getContents(), 'entity_field', TRUE);
      }
      else {
        $rep = new ValueRepresentation('Field', $out, 'entity_field', TRUE);
      }
      $v->addRepresentation($rep, 0);
    }

    return $v;
  }

  /**
   * Attach fields to FieldableEntity.
   */
  private function parseFieldableEntity(FieldableEntityInterface $entity, InstanceValue $v): InstanceValue {
    $ap = $v->getContext()->getAccessPath();
    $contents = [];
    $fields = $entity->getFields();

    $inNodePluginStash = $this->currentlyInPlugin;
    try {
      $this->currentlyInPlugin = TRUE;

      foreach ($fields as $name => $property) {
        $values = $property->getValue();
        if (!is_array($values)) {
          return $v;
        }

        $context = new ClassOwnedContext($name, $v->getClassName());
        $context->depth = $v->getContext()->getDepth() + 1;
        if (NULL !== $ap) {
          $context->access_path = $ap . '->' . $name;
        }

        if (1 === count($values) && 1 === \count($values[0])) {
          if ($context->access_path !== NULL) {
            $context->access_path .= '->' . \key($values[0]);
          }
          $values = \reset($values[0]);
          $contents[] = $this->parser->parse($values, $context);
        }
        else {
          $contents[] = $this->parser->parse($property, $context);
        }
      }
    } finally {
      $this->currentlyInPlugin = $inNodePluginStash;
    }

    if ($contents) {
      $rep = new ContainerRepresentation('Entity Fields', $contents, 'entity_fields');
      $v->addRepresentation($rep, 0);
    }

    return $v;
  }

}
