<?php

/**
 * @file
 * This file is loaded by composer before drupal starts.
 */

use Kint\Kint;

Kint::$enabled_mode = FALSE;
